## Developing Helm Charts

This directory is dedicated to managing Helm Charts for a given application.

#### Useful Resources

*  https://docs.helm.sh/developing_charts
*  https://github.com/hnykda/scalable-django-helm-chart
*  https://traefik.io/
*  https://github.com/helm/charts/tree/master/stable/traefik
*  https://github.com/helm/charts/tree/master/stable/redis
*  https://github.com/helm/charts/tree/master/stable/postgresql
*  https://github.com/dockerfiles/django-uwsgi-nginx
*  https://hub.docker.com/u/arm32v7/

#### Kuard

```
kubectl run kuard-arm1 --image=davidpr90019/kuard-arm:1 --port=8080 --expose=true
kubectl port-forward kuard 8080:8080
kubectl get pods -o wide
kubectl run redis --image=arm32v7/redis --port=6379
kubectl create service clusterip redis --tcp=6379:6379
kubectl run postgres --image=arm32v7/postgres --env="POSTGRES_PASSWORD=password"
# The following will allow apps in the cluster to connect to redis via: redis:5432
# - I am unsure if the service name or the app name makes the app/service connect.
kubectl create service clusterip postgres --tcp=5432:5432
kubectl run python --image=arm32v7/python
```

https://github.com/kubernetes-up-and-running/kuard/issues/5