from django import forms

from . import models


class SellForm(forms.ModelForm):
    """
    best way to show the sell form
    """
    stock_ticker = forms.ChoiceField(choices=[])

    def __init__(self, username, **kwargs):
        super().__init__(**kwargs)
        self.fields['stock_ticker'].choices = self._meta.model.objects.unsold(username)

    class Meta:
        model = models.Transaction
        fields = ('stock_ticker', 'quantity',)
