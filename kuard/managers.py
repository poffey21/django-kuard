from django.db import models


class TransactionQuerySet(models.QuerySet):

    def unsold(self, username):
        return self.filter(user__username=username)
