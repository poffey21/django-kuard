from django.db import models

from . import managers


class Transaction(models.Model):
    """
    Keep track of whether or not a stock has
    been bought or sold
    """
    TYPES = (
        ('buy', 'Market Buy'),
        ('sell', 'Market Sell'),
    )
    STATUSES= (
        ('pending', 'Pending'),
        ('filled', 'Filled'),
    )
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    stock_ticker = models.CharField(max_length=4)
    type = models.CharField(max_length=4, choices=TYPES)
    submitted = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=8, choices=STATUSES)
    quantity = models.IntegerField()
    filled = models.DateTimeField(blank=True, null=True)
    amount = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    total = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)

    objects = managers.TransactionQuerySet.as_manager()

    def __str__(self):
        return self.stock_ticker