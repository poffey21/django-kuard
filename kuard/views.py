from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import generic

from . import forms
from . import models


@method_decorator(login_required, name='dispatch')
class HomeView(generic.ListView):
    model = models.Transaction


@method_decorator(login_required, name='dispatch')
class BuyView(generic.CreateView):
    model = models.Transaction
    fields = ('stock_ticker', 'quantity', )
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class SellView(generic.CreateView):
    model = models.Transaction
    form_class = forms.SellForm
    success_url = reverse_lazy('home')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['username'] = self.request.user.username
        return kwargs
