# django-kuard

Attempt to create a Django based showcase of K8s


### Entry Points

* uWSGI + Django: Web App

```
uwsgi --uid 999 \
      --gid 999 \
      --chdir /srv/www \
      --wsgi-file www/wsgi.py \
      --env DJANGO_SETTINGS_MODULE=www.settings \
      --http :8000 \
      --thunder-lock \
      --enable-threads \
      --processes 4 \
      --threads 2 \
      --offload-threads 4;
```

* Celery + Django: Backend Listener App

```
celery 
```

* Django App: Scheduled Job App

```
DJANGO_SETTINGS_MODULE=www.settings django-admin sync
```

**Base Image:**: https://gitlab.com/poffey21/docker-django/